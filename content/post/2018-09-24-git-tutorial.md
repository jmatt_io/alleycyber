---
title: Git Basics
subtitle: An Introduction to Git
date: 2018-09-24
tags: ["git", "code"]
---

In this article I'll be walking you through the basics of git. This is really geared towards the absolute beginners who have never used or heard of git. 

Git cannot be defined without explaining what version control is. Version control is a way developers use to track and manage changes on a project. For example a large company wouldn’t edit their production code to add features. Instead they would branch to ensure they are only working on the source code separate from the official code and merge it once the “feature” is finished. Once the project is merged to master the source code is considered official. These changes are tracked and revered if there is a need. 

Git is the most widely used version control system which helps tracking changes in files and aids in coordinating work among multiple people. It was developed in 2005 by Linus Torvalds. Some things you can do using git are find out:
* Changes that were made. 
* Who made the changes
* When the changes were made
* Why changes were made (depending on commit message)

With some of the theory covered lets dive into this.

For the sake of this demo we will be using Gitlab to maintain our imaginary codebase. But you are welcome to use Github or Bitbucket or any other preferred tools. All these utilize git so the information will be standard amongst all of these tools. 

To make things a bit more hands on we’ll be using a unix terminal for pushing code. Windows people download WSL from the windows store and there are plenty of instructions available online on how to get that setup. Once you’ve got this setup you can follow along. 

For people who don’t currently have an SSH key let’s generate one. Open up your terminal and run the command below. 

`ssh-keygen -t ed25519`
       
Lets change our directory to .ssh folder and print out what our SSH public key looks like. 
`cd .ssh` 
`cat id_ed25519.pub`


Let’s login to Gitlab and upload our public key and you are all set up to use git! 

*Git Command Ref* 

Create a new empty repo or initialize a existing folder as repo 
`git init `

Create a new branch and switch to it.<br> 
`git checkout -b [name_of_your_new_branch]`

Create a new branch <br>
`git branch [name_of_your_new_branch]`

Change working branch <br>
`git checkout [name_of_your_new_branch]`

Push your branch to github/gitlab <br>
`git push origin [name_of_your_new_branch]`

see all branches <br>
`git branch`

pull updates from your remote branch to master. <br>
`git pull origin master`

diff for Change <br>
`git diff`

Check status of what's changed or what you've committed <br>
`git status`

commit a change and comment <br>
`git commit -m "your_message_here"`

pull changes from remote server but keep your changes locally and merge. <br>
`git pull --rebase`

push your changes to master <br>
`git push origin master`

find out who changed what line of a file <br>
`git blame [Filename]`

show details of last commit <br>
`git show` 

Show a log of all git related activities <br>
`git log` 
