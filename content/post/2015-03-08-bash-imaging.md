---
title: Wipe and Image Script
subtitle: using bash to wipe and image computer
date: 2015-03-08
tags: ["bash", "imaging" , "code"]
---

The below code was designed to image a computer using a prebuilt image created using dd. 

For those who have not used dd before it is a command line utlity for unix used to convert and copy files. Typically dd is used for tasks such as backing up boot sectors of a drive, or copying a fixed amount of random data. 

The script walks through how to image a computer using the created image using dd hosted on a network drive. For my case I used a cifs share hosted locally. 
<!--more-->


Heres the imaging script. 
_Note I've added an extra step to make it easier to list out drives if you are using this to image Mac's_:

{{< highlight bash "linenos=inline">}}
    #!/bin/bash
    #author: Jeff
    #website: http://jmatt.io
    #website: https://alleycyber.com
    #twitter: @jmatt_io
    TMPDIR="/tmp/images"
    echo "System Imaging Script"
    echo -e "Mounting Local Drive ${DISK}..."
    while true; do
        read -p "Are you Running a Mac?" yn
        case $yn in
            [Yy]* ) echo "You have specified your system as Mac" && diskutil list;;
            [Nn]* ) echo "You have specified your system as Linux" && fdisk -l;;
            * ) echo "Please answer yes or no.";;
        esac
        break
    done
    echo "################################################################################################"
    echo "Type the disk name, followed by [ENTER]: ex.(disk2)"
    read disk
    echo "################################################################################################"
    echo "Would you like to wipe your drive using "urandom" or "zero"? Please type and press [ENTER]:"
    read wpefrm
    sudo dd if=/dev/$wpefrm of=/dev/$disk
    echo -e "Wiping Complete"
    echo "################################################################################################"
    #makes a temporary directory on the host system to mount network storage
    sudo mkdir ${TMPDIR} ||
    { echo "Failed creating $TMPDIR."; exit 1; }
    #mounts network storage. you can change this to ftp aswell.
    sudo mount.cifs //drive/source -o username=admin,password=password, ${TMPDIR}
    #wipes the drive.
    #starts imaging the drive selected above
    dd if=${TMPDIR}/Image1 of=/dev/$disk
    echo -e "Image Complete"
    ignored="" #ignored variable
    #powers off or ignores the shutdown command.
    read -s -r -p "Press any key to power this machine off" -n 1 ignored
    sudo shutdown
{{</ highlight >}}