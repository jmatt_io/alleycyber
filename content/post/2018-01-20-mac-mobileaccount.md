---
title: Creating a Mac Mobile Account
subtitle: Mac Mobile account creation fun
date: 2018-01-20
tags: ["bash", "mac" , "mobileaccount", "Windows", "Domain", "DomainUsers"]
---

The code below was written to aide creating Mac mobile accounts for domain joined users a bit faster. This will help ensure your user is added to the computer with a mobile account and they are added as an admin.<!--more-->


Heres the  script. 

{{< highlight bash "linenos=inline">}}
#!/bin/bash
#Mac_OS Setup Script
read -p "Enter username: " USER
while true; do
    read -p "Is the Mac joined to the Domain?" yn
    case $yn in
        [Yy]* ) echo "You have specified the system is joined to the Domain" &&
        echo "ADDING SECURITY OU MEMBERS"
        sudo /System/Library/CoreServices/ManagedClient.app/Contents/Resources/createmobileaccount -n $USER
        sudo dscl . -append /Groups/admin GroupMembership $USER
        ;;
        [Nn]* ) echo "Go back and join the computer to the domain";;
        * ) echo "Please answer yes or no.";;
    esac
    break
done
{{</ highlight >}}